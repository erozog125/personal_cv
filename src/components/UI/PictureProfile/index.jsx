/* PictureProfile UI Component */
import React from 'react';
import { Main } from './styles';

export const PictureProfile = ({src, alt}) => {
  return(
    <>
    <Main>
      <img src={src} alt={alt} />
    </Main>  
    </>
  )
}