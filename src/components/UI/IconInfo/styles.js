import styled from 'styled-components';

export const Main = styled.div `
  img {
    height: 1.9em; 
    width: 1.9em;
    display: flex;
    justify-content: center;
    margin: 0.3em 1em 0 0.5em;            
  }  
`;