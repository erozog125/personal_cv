import styled from 'styled-components';

export const Main = styled.div `
  background-color: #616362;
  border: groove white 5px;
  border-radius: 1em 3.5em;
  color: white;
  height: 40vw;
  margin: 2em;
  padding: 1.5em 0 1.5em 1.5em;
  margin: 0.5em 0 0 1em;

  h1 {
    font-size: 2em;
  }

  p {
    font-size: 1.5em;
  }

  li {
    font-size: 1.1em;
    list-style: none;
  }

  img {
    height: 1.8em; 
    margin: 0.3em 1em 0 0.5em;        
    width: 1.8em;
  } 

  p {
    margin: 0;
    padding: 0.5em;
  }

  @media (min-width: 200px) and (max-width: 500px) {
 width: 55vw;
 height: 73vw;
 font-size: 8px;
 justify-content: center;

 ul {
   padding: 0;
 }
}
`;

