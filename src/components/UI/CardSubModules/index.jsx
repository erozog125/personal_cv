import React from 'react';
import {Main} from './styles';

export const CardSubModules = ({src, alt, title, description,item1,item2,item3,item4,item5,item6,item7,item8,item9}) => {
  return(
    <>
      <Main>
        <div>
          <img src={src} alt={alt} />
          <h1>{title}</h1>
        </div>
        <p>{description}</p>
        <ul>
          <li>{item1}</li>
          <li>{item2}</li>
          <li>{item3}</li>
          <li>{item4}</li>          
          <li>{item5}</li>          
          <li>{item6}</li>          
          <li>{item7}</li>          
          <li>{item8}</li>          
          <li>{item9}</li>          
        </ul>
      </Main>
    </>
  )
}