import styled from 'styled-components';

export const Main = styled.div `
  
  h1 {
    padding: 0.3em 0;
    font-size: 1.3em;
    color: white;
    font-weight: bold;
  }
  
  h2,p,a {
    font-size: 1.2em;
    color: white;
    font-weight: bold;
    padding: 0;
    line-height: 1.15em;
  }
`;