/* PersonalInfo UI Component */
import React from 'react';
import { Main } from './styles';

export const PersonalInfo = ({name, profession, email, phone, city, age, route}) => {
  return(
    <>
      <Main>
        <h1>{name}</h1>
        <h2>{profession}</h2>  
        <p>{phone}</p>
        <p>{email}</p>
        <p>{age}</p>
        <p>{city}</p>
        <a target='_blank' rel="noopener noreferrer" href={route}>Portafolio Gitlab</a>
      </Main>
    </>
  )
}