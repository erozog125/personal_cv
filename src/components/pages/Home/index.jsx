/* home page Component */
import { HeadHome } from '../../layout/HeadHome';
import React from 'react';
import { Container } from '../../layout/Container';


export const Home = () =>{
  return(
    <>
      <HeadHome />
      <Container />
    </>
  )
}
