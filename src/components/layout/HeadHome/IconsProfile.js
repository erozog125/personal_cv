export const icons = [

  {
    src: '/icons/home/name.svg',
    alt: 'name',
  },
  {
    src: '/icons/home/dev.svg',
    alt: 'profession',
  },
  {
    src: '/icons/home/whatsapp.png',
    alt: 'Whatsapp',
  },
  {
    src: '/icons/home/email.png',
    alt: 'Email',
  },
  {
    src: '/icons/home/calendar.png',
    alt: 'Age',
  },  
  {
    src: '/icons/home/home.png',
    alt: 'home',
  },  
  {
    src: '/icons/home/git.svg',
    alt: 'folder',
  },
];