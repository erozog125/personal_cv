/*Styles HeadHome layout component*/
import styled from 'styled-components';

export const Main = styled.article`
  background-color: #038779;
  height: 30vw;
  padding: 1.1em;

  @media (min-width: 200px) and (max-width: 500px)  {
    width: 100%;
    height: 350px;

    img {
      display: none;
    }
}  
`;