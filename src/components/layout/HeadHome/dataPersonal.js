export const dataPersonal = [
    {
      name: 'Edwin Gustavo Rozo Gómez',
      profession: 'Software engineer',
      age: 33,
      phone: '3164904568',
      email: 'erozog125@gmail.com',
      city: 'Armenia-Quindío',
      route: 'https://gitlab.com/erozog125/personal_cv.git',
    }];