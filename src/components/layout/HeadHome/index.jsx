/* HeadHome layout Component*/
import React from 'react';
import { Col, Row }  from 'react-bootstrap';
import { IconInfo } from '../../UI/IconInfo/index';
import { icons } from './IconsProfile';
import { PersonalInfo } from '../../UI/PersonalInfo/index';
import { dataPersonal } from './dataPersonal';
import { PictureProfile } from '../../UI/PictureProfile/index';
import profile from '../../../assets/images/home/profile.jpg';
import { Main } from './styles';


export const HeadHome = () => {
  return(
    <>
      <Main>
        <Row className = 'justify-content-md-center'>
          <Col xs={12} sm={1}>
            {icons.map((icon) => (
              <IconInfo
                className = 'icons'
                src = {icon.src}
                alt = {icon.alt}
              />
          ))}
          </Col>
          <Col xs={7}>
            {dataPersonal.map((info) => (
              <PersonalInfo 
                className = 'personalInfo'
                name={info.name}
                profession={info.profession}
                age={info.age}
                phone={info.phone}
                email={info.email}
                city={info.city}
                route={info.profession}
              />
            ))}
            </Col>
            <Col xs={4}>
              <PictureProfile
                className='pictureProfile'
                src={profile}
                alt='Imagen Perfil'
              />
            </Col>
        </Row>
      </Main>  
    </>
  );
}