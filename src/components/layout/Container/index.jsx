import React from 'react';
import {info} from './ModulesInfo';
import { CardSubModules } from '../../UI/CardSubModules/index';
import { Col, Row } from 'react-bootstrap';
import { Main } from './styles';

export const Container = () => {
  return(
    <>
      <Row around='xs'>
      <Main>
          {info.map((info) => (
            <Col xs={12} md={6} key = {info.key} className = 'subModules' >  
              <CardSubModules
                  src={info.src}
                  alt={info.alt}
                  title={info.title}
                  description={info.description}
                  item1={info.item1}
                  item2={info.item2}
                  item3={info.item3}
                  item4={info.item4}
                  item5={info.item5}
                  item6={info.item6}
                  item7={info.item7}
                  item8={info.item8}
                  item9={info.item9}
                />
            </Col>
            ))}                      
        </Main>
      </Row>      
    </>
  )
}