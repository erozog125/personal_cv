import styled from 'styled-components';

export const Main = styled.div ` 
  
  .subModules {
    display: flex;
  }

  div {
    justify-content: center;
    width: 100%;
    font-size: 0.8em;
    padding: 0.8em;     
  }

  ul {
    font-size: 1em;    
  } 
`;